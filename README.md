# PGPS Iconpoint

PGPS Iconpoint is a generic module that shows an icon on top of the viewer.

## Use Case

This module was created to replace old PNG-based solution where the users couldn't manage the icons themselves.
With this module the user can create his own icons in Drupal and place them to the process by latitude and longitude or by virtual coordinates.

## How to use

Enabling the module and features will provide a content type for the icon and the locationtype itself. Adding icons to the platform will make them available for the locationtype. The module also provides a basic configuration page for the sizing of the icon.
