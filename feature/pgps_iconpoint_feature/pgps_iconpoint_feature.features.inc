<?php

/**
 * @file
 * pgps_iconpoint_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pgps_iconpoint_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pgps_iconpoint_feature_node_info() {
  $items = array(
    'icon' => array(
      'name' => t('Icon'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_pglocation_defaults().
 */
function pgps_iconpoint_feature_pglocation_defaults() {
    $types = [];
    $types['icon_point'] = [
        'type' => 'icon_point',
        'name' => 'Icon point',
    ];
    return $types;
}
