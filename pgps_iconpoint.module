<?php

namespace { // global

	/**
	 * Implements hook_menu
	 */
	function pgps_iconpoint_menu() {
		return [
			'admin/config/pgcore/pgps_iconpoint' => [
				'title' => 'Pgps_iconpoint Configuration',
				'description' => 'Configure the pgps_iconpoints',
				'page callback' => 'drupal_get_form',
				'page arguments' => [ 'pgps_iconpoint\adminform' ],
				'access arguments' => [ 'administer site configuration' ],
				'type' => MENU_NORMAL_ITEM,
			],
		];
	}

	/**
	 * Drupal hook for fetching the location data for the icon.
	 */
	function pgps_iconpoint_icon_point_pgdata_location($location) {
		$field_icon = $location->field_icon->value();
		$field_icon_file = $location->field_icon->field_icon_file->value();
		$field_icon_size = $location->field_icon->field_icon_size->value();
		$field_panel_image = $location->field_icon->field_panel_image->value();
		$field_panel_description = $location->field_icon->field_panel_description->value();
		$field_panel_documents = $location->field_icon->field_panel_documents->value();
		$field_external_system_url = "";
		if ($location->field_icon->field_external_system_url->value() !== null) {
			$field_external_system_url = $location->field_icon->field_external_system_url->value();
		}

		$files = [];
		foreach ($field_panel_documents as $file) {
			$files[$file["filename"]] = file_create_url($file["uri"]);
		}

		$info = [
			"title"						=> $field_icon->title,
			"nid"						=> $field_icon->nid,
			"field_icon_file_filename"	=> $field_icon_file["filename"],
			"field_icon_file_url"		=> file_create_url($field_icon_file["uri"]), // absolute URL of the icon
			"size"						=> $field_icon_size,
			"field_panel_description"	=> $field_panel_description,
			"field_panel_image"			=> file_create_url($field_panel_image["uri"]), // absolute URL of the panel image
			"field_panel_documents"		=> $files,
			"field_external_system_url"	=> $field_external_system_url,
		];
		return $info;
	}

	/**
	 * Implements hook_pginit().
	 */
	function pgps_iconpoint_pginit($path) {
		$here = drupal_get_path("module", "pgps_iconpoint");
		drupal_add_js('pgps_iconpoint = {};', 'inline');
		drupal_add_js('pgps_iconpoint.CONFIGURATION = ' . json_encode(pgps_iconpoint\config()) . ';', 'inline');
		drupal_add_js("$here/client/js/pgps_iconpoint.js");
		drupal_add_css("$here/client/css/pgps_iconpoint.css");
	}
}

namespace pgps_iconpoint {
	function adminform() {
		return system_settings_form([
			'pgps_iconpoint_icon_size' => [
				'#type' => 'textfield',
				'#attributes' => [
					// The space before "type" is mandatory
					' type' => 'number',
				],
				'#title' => 'Icon size',
				'#description' => 'Configure the size of the icons. This will set the container width and the height, the image is set to "cover" the container.',
				'#default_value' => variable_get('pgps_iconpoint_icon_size', 28),
			],
		]);
	}

	function config() {
		return [
			'iconSize' => variable_get('pgps_iconpoint_icon_size', 28),
		];
	}
}

