subthemes_path_prefix = "../../../pgtheme/themes/"
subthemes_path_suffix = "/sass/"
subtheme_name = ""
ENV.each do|a|
	if a[0] == "SUBTHEME"
		subtheme_name = a[1]
	end
end
# failing imports cause crashes so import our empty one
if subtheme_name == ""
	subtheme_name = "default-theme"
end
add_import_path "../../../pgtheme/sass/" # main theme folder
add_import_path subthemes_path_prefix + subtheme_name + subthemes_path_suffix # subtheme folder

require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
sass_dir = "sass"
css_dir = "client/css"
images_dir = "client/css/img"
javascripts_dir = "client/js"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_stype = :compressed
# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass