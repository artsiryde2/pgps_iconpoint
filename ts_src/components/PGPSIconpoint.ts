/// <reference path="../../../pguikit/client/js/pguikit.d.ts" />
namespace pgps_iconpoint {
	@Component<VPGPSIconpoint>({
		template: /* html */ `
			<div class="pgps_iconpoint__container">
				<img
					:title="title"
					alt="iconpoint"
					:src="locationData.field_icon_file_url"
					class="pgps_iconpoint__container__image"
					:style="iconSizeStyle"
					:class="customClass"
					@click="clickEvent"
				/>
			</div>
		`,
		props: ["title", "locationData", "customClass"],
	})

	class VPGPSIconpoint extends Vue {
		protected readonly title: string;
		protected readonly locationData: IIconpoint;
		protected readonly customClass: string;

		protected get iconSizeStyle(): Partial<CSSStyleDeclaration> {
			const iconSize = this.locationData.size
				? this.locationData.size
				: CONFIGURATION.iconSize.toString();

			return {
				width: `${iconSize}px`,
				height: `${iconSize}px`,
			};
		}
		protected panel: pgcore.UI.Block | undefined = undefined;

		protected openPanelEvent: PGEvent<string, void, void> = new PGEvent<string, void, void>();

		public mounted(): void {
			const element: HTMLElement = document.createElement("div");
			const imgElement: HTMLElement = document.createElement("img");
			imgElement.setAttribute("src", this.locationData.field_panel_image);

			Object.keys(this.locationData.field_panel_documents).forEach((key: string) => {
				const linkElement: HTMLElement = document.createElement("a");
				linkElement.setAttribute("href", this.locationData.field_panel_documents[key]);
				linkElement.append(key);
				$(element).append(linkElement);
			});

			$(element).append(this.locationData.field_panel_description);
			$(element).append(imgElement);

			const options: pgcore.UI.IBlockOptions = {
				content: element,
				title: this.title,
				displayer: new pgcore.UI.DraggablePanelDisplayer(),
			};
			this.panel = new pgcore.UI.Block(options);
		}

		protected clickEvent(): void {
			if (this.locationData.field_external_system_url.length > 0) {
				window.open(this.locationData.field_external_system_url, "_blank");
			} else {
				if (this.panel) {
					this.panel.show();
				}
			}
		}
	}

	export class PGPSIconpoint extends viewer.Marker {
		private readonly title: string;
		private readonly locationData: IIconpoint;
		private marker: VPGPSIconpoint;
		private customClass: string;

		public constructor(
			title: string,
			locationData: IIconpoint,
			location: viewer.MarkerPosition,
			customClass: string
		) {
			super(location);
			this.title = title;
			this.locationData = locationData;
			this.customClass = customClass;
		}

		protected generateElement(): JQuery {
			this.marker = new VPGPSIconpoint({
				data: {
					title: this.title,
					locationData: this.locationData,
					customClass: this.customClass,
				},
			}).$mount();
			return $(this.marker.$el);
		}

		public handleDestroy(): void {
			this.marker.$destroy();
		}
	}
}
