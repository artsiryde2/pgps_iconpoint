/// <reference path="../../../client/js/pgcore.d.ts" />
/// <reference path="../../../components/viewer/client/js/viewerpage.d.ts" />

/**
 * PGPS Iconpoint module
 */
namespace pgps_iconpoint {
	type Configuration = {
		iconSize: number;
	};

	export declare const CONFIGURATION: Configuration;

	export interface IIconpoint {
		readonly title: string;
		readonly nid: number;
		readonly size: string | null;
		readonly field_icon_file_filename: string;
		readonly field_icon_file_url: string;
		readonly field_panel_description: string;
		readonly field_panel_image: string;
		readonly field_panel_documents: { [key: string]: string };
		readonly field_external_system_url: string;
	}

	const MODN: "pgps_iconpoint" = "pgps_iconpoint";
	const LOCATION_TYPE: "icon_point" = "icon_point";

	/**
	 * Custom query location that contains zero or more custom query field collection items.
	 */
	type IconpointLocation = pgdb.ILocationObj & {
		[MODN]: IIconpoint;
	};

	/**
	 * Helper function to check typings for location object.
	 *
	 * @param i pgDB location object.
	 *
	 * @returns True if location is type of custom_query_location, false if not.
	 */
	function isIconpointLocation(i: pgdb.ILocationObj): i is IconpointLocation {
		return (<IconpointLocation>i).type === LOCATION_TYPE;
	}

	viewerpage.init.on((vpage): void => {
		vpage.locationLoaded.on((loc): void => {
			vpage.view.ready.once((view): void => {
				if (!isIconpointLocation(loc.location)) {
					return;
				}

				const { location } = loc;
				const marker = new PGPSIconpoint(
					location.title,
					location[MODN],
					viewer.Marker.createMarkerLocation(loc.location),
					loc.custom_class
				);

				view.addMarker(marker, {
					layer: loc.layer,
				});

				vpage.destructing.on((): void => {
					marker.handleDestroy();
				});
			});
		});
	});
}
